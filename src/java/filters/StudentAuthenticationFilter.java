package filters;

import jakarta.servlet.*;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.JwtTokenProvider;

import java.io.IOException;

@WebFilter(filterName = "StudentAuthenticationFilter",
        urlPatterns = {""},
        dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD})
public class StudentAuthenticationFilter implements Filter {
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        jwtTokenProvider = new JwtTokenProvider();
        Filter.super.init(filterConfig);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // Check cookie has session ID or not
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        Cookie[] cookies = req.getCookies();
        if(cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("student_token")) {
                    if(jwtTokenProvider.validateToken(cookie.getValue())){
                        if(jwtTokenProvider.getRoleFromJWT(cookie.getValue()).equals("2")) {
                            filterChain.doFilter(servletRequest, servletResponse);
                            return;
                        }
                    } else {
                        Cookie newCookie = new Cookie("manager_token", "");
                        newCookie.setMaxAge(0);
                        newCookie.setDomain("localhost");
                        newCookie.setPath("/FPT-Nihongo");
                        newCookie.setHttpOnly(true);
                        res.addCookie(newCookie);
                        break;
                    }
                }
            }
        }
        res.sendRedirect("login");
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }

}
