package controllers;

import entities.Manager;
import entities.Student;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import services.ManagerService;
import services.JwtTokenProvider;
import services.StudentService;

import java.io.IOException;

@WebServlet(name="LoginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {
    private final Integer COOKIE_LIFE_TIME = 60 * 60 * 24;
    private ManagerService managerService;
    private StudentService studentService;
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void init() throws ServletException {
        managerService = ManagerService.getInstance();
        studentService = StudentService.getInstance();
        jwtTokenProvider = new JwtTokenProvider();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("login.html");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        Manager manager = managerService.getAccountByEmailAndPassword(username, password);
        Student student = studentService.getStudentByEmailAndPassword(username, password);
        if(manager != null){
            Cookie cookie = new Cookie("manager_token", jwtTokenProvider.generateToken(manager));
            cookie.setMaxAge(COOKIE_LIFE_TIME);
            cookie.setDomain("localhost");
            cookie.setPath("/FPT-Nihongo");
            cookie.setHttpOnly(true);
            resp.addCookie(cookie);
            resp.sendRedirect("admin/index.html");
        } else if (student != null){
            Cookie cookie = new Cookie("student_token", jwtTokenProvider.generateToken(student));
            cookie.setMaxAge(COOKIE_LIFE_TIME);
            cookie.setDomain("localhost");
            cookie.setPath("/FPT-Nihongo");
            cookie.setHttpOnly(true);
            resp.addCookie(cookie);
            resp.sendRedirect("index.html");
        }
    }
}
