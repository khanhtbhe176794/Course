CREATE TABLE admin (
  admin_id INT PRIMARY KEY,
  email VARCHAR(50) NOT NULL,
  active BIT DEFAULT 1,
  password VARCHAR(50) NOT NULL
);

CREATE TABLE instructor (
  instructor_id INT PRIMARY KEY,
  active BIT DEFAULT 1,
  password VARCHAR(50) NOT NULL,
  full_name VARCHAR(100) NOT NULL,
  specialization VARCHAR(100),
  email VARCHAR(100) NOT NULL
);

CREATE TABLE student (
  student_id INT PRIMARY KEY,
  email VARCHAR(100) NOT NULL,
  active BIT DEFAULT 1,
  password VARCHAR(50) NOT NULL,
  full_name VARCHAR(100) NOT NULL,
  date_of_birth DATE,
  enrollment_date DATE
);

CREATE TABLE manager (
  manager_id INT PRIMARY KEY,
  email VARCHAR(100) NOT NULL,
  active BIT DEFAULT 1,
  password VARCHAR(50) NOT NULL,
  full_name VARCHAR(100) NOT NULL
);

CREATE TABLE category_course(
  category_id INT PRIMARY KEY,
  category_name VARCHAR(50) NOT NULL
);

CREATE TABLE course(
  course_id INT PRIMARY KEY,
  course_name VARCHAR(50) NOT NULL,
  img_URL VARCHAR(255) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  description VARCHAR(250) NOT NULL,
  category_id INT NOT NULL,
  instructor_id INT NOT NULL,
  FOREIGN KEY (category_id) REFERENCES category_course(category_id),
  FOREIGN KEY (instructor_id) REFERENCES instructor(instructor_id)
);

CREATE TABLE lecture(
  lecture_id INT PRIMARY KEY,
  lecture_name VARCHAR(50) NOT NULL,
  description VARCHAR(250) NOT NULL,
  image_url VARCHAR(255) NOT NULL,
  video_url VARCHAR(255) NOT NULL,
  course_id INT NOT NULL,
  FOREIGN KEY (course_id) REFERENCES course(course_id)
);

CREATE TABLE exam(
  exam_id INT PRIMARY KEY,
  exam_name VARCHAR(50) NOT NULL,
  description VARCHAR(250) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  course_id INT NOT NULL,
  FOREIGN KEY (course_id) REFERENCES course(course_id)
);

CREATE TABLE grade(
  grade_id INT PRIMARY KEY,
  student_id INT NOT NULL,
  exam_id INT NOT NULL,
  score DECIMAL(10,2) NOT NULL,
  FOREIGN KEY (student_id) REFERENCES student(student_id),
  FOREIGN KEY (exam_id) REFERENCES exam(exam_id)
);

CREATE TABLE order_table(
  order_id INT PRIMARY KEY,
  student_id INT NOT NULL,
  order_date DATE NOT NULL,
  total DECIMAL(10,2) NOT NULL,
  status VARCHAR(50) NOT NULL,
  notes VARCHAR(50) NOT NULL,
  FOREIGN KEY (student_id) REFERENCES student(student_id)
);

CREATE TABLE order_detail(
  detail_id INT PRIMARY KEY,
  order_id INT NOT NULL,
  course_id INT NOT NULL,
  price DECIMAL(10,2) NOT NULL,
  quantity INT NOT NULL,
  FOREIGN KEY (order_id) REFERENCES order_table(order_id),
  FOREIGN KEY (course_id) REFERENCES course(course_id)
);

CREATE TABLE enrollment (
  enrollment_id INT PRIMARY KEY,
  student_id INT NOT NULL,
  course_id INT NOT NULL,
  enrollment_date DATE NOT NULL,
  FOREIGN KEY (student_id) REFERENCES student(student_id),
  FOREIGN KEY (course_id) REFERENCES course(course_id)
);
CREATE TABLE category_blog(
  category_id INT PRIMARY KEY,
  category_name VARCHAR(50) NOT NULL
);
CREATE TABLE blog (
  blog_id INT PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  content TEXT NOT NULL,
  publish_date DATE NOT NULL,
  manager_id INT NOT NULL,
  category_id INT NOT NULL,
  FOREIGN KEY (manager_id) REFERENCES manager(manager_id),
  FOREIGN KEY (category_id) REFERENCES category_blog(category_id)
);

CREATE TABLE blog_details (
  blog_id INT PRIMARY KEY,
  views INT DEFAULT 0,
  content TEXT NOT NULL,
  likes INT DEFAULT 0,
  FOREIGN KEY (blog_id) REFERENCES blog(blog_id)
);

CREATE TABLE instructor_salary (
  salary_id INT PRIMARY KEY,
  instructor_id INT NOT NULL,
  manager_id INT NOT NULL,
  amount DECIMAL(10,2) NOT NULL,
  pay_date DATE NOT NULL,
  status VARCHAR(50) NOT NULL,
  FOREIGN KEY (instructor_id) REFERENCES instructor(instructor_id),
  FOREIGN KEY (manager_id) REFERENCES manager(manager_id)
);

