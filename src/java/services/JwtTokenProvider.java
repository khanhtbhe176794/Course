package services;

import entities.IActor;
import io.jsonwebtoken.*;
import java.util.Date;

public class JwtTokenProvider {
    private final String JWT_SECRET = "U2FsdGVkX1/1nMZ54TUV7X9sdcY79Pi29MptWQ2xwQ0=";

    private final long JWT_EXPIRATION = 86400000L; // 1 day in milliseconds

    public String generateToken(IActor iActor) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + JWT_EXPIRATION);

        // Create a Claims object and set the necessary claims
        Claims claims = Jwts.claims();
        claims.setSubject(iActor.getId().toString());
        claims.put("role", iActor.getRoleId().toString());
        claims.setIssuedAt(now);
        claims.setExpiration(expiryDate);

        // Build and return the JWT
        return Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, JWT_SECRET)
                .compact();
    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public String getRoleFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(JWT_SECRET)
                .parseClaimsJws(token)
                .getBody();

        return (String) claims.get("role");
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JWT_SECRET).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            System.out.println("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            System.out.println("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            System.out.println("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            System.out.println("JWT claims string is empty.");
        }
        return false;
    }
}