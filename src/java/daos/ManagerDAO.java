package daos;

import entities.Manager;
import mappers.impl.ManagerMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ManagerDAO extends GenericDAO<Manager> {

    private List<Manager> listManager;

    public ManagerDAO (){
        listManager = new ArrayList<>();
        listManager.add(new Manager(1, "Hoang Anh Quoc", "anhquoc@gmail.com", "123456", "", true, "", "", 1));
    }

    private final String SELECT_ADMIN_BY_EMAIL_AND_PASSWORD = "SELECT * FROM admin WHERE email = ? AND password = ?";
    public List<Manager> getManagerByEmailAndPassword(String email, String password){
//        List<Manager> result = executeQuery(SELECT_ADMIN_BY_EMAIL_AND_PASSWORD, new AdminMapper(), email, password);
//        if (result == null || result.isEmpty()){
//            return null;
//        }
//        return result;

        return listManager.stream()
                .filter(m -> m.getEmail().equals(email) && m.getPassword().equals(password))
                .collect(Collectors.toList());
    }
}
