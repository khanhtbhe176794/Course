CREATE DATABASE SWP391G3;

USE SWP391G3;

CREATE TABLE [role] (
    role_id INT PRIMARY KEY,
    role_name VARCHAR(50) not null
);


CREATE TABLE [admin] (
    admin_id INT PRIMARY KEY,
    admin_name VARCHAR(50) not null,
	email varchar(50) not null,
	[password] varchar(50) not null,
	[address] varchar(50) not null,
	gender bit not null,
	img_URL varchar(255) not null,
	phone varchar(50)not null,
    role_id INT not null,
    FOREIGN KEY (role_id) REFERENCES role(role_id)
);


CREATE TABLE [user] (
    [user_id] INT PRIMARY KEY,
    [user_name] VARCHAR(50) not null,
	email varchar(50) not null,
	gender bit not null,
	phone varchar(50) not null,
	img_URL varchar(255) not null,
    role_id INT not null,
    FOREIGN KEY (role_id) REFERENCES role(role_id)
);

Create table Category(
    category_id INT PRIMARY KEY,
	categoryname varchar(50) not null
)

create table Course(
    course_id INT PRIMARY KEY,
	coursename varchar(50) not null,
	price decimal(10,0) not null,
	[description] varchar(250) not null,
	create_date date not null,
	category_id INT not null,
    admin_id INT not null,
	img_URL varchar(255) not null,
	video_URL varchar(255) not null,
    FOREIGN KEY (category_id) REFERENCES Category(category_id),
    FOREIGN KEY (admin_id) REFERENCES Admin(admin_id)
);


create table Lecture(
   lecture_id INT PRIMARY KEY,
   lecturename varchar(50) not null,
   [description] varchar(250) not null,
   image_url VARCHAR(255) not null,
   video_url VARCHAR(255) not null,
   course_id INT not null,
   FOREIGN KEY (course_id) REFERENCES Course(course_id)
   )

create table reviews (
   reviews_id INT PRIMARY KEY,
   reviews_comment varchar(50),
   [user_id] INT not null,
   course_id INT not null,
   admin_id INT not null,
   parent_id INT null,
   FOREIGN KEY (parent_id) REFERENCES reviews(reviews_id),
   FOREIGN KEY (course_id) REFERENCES Course(course_id),
   FOREIGN KEY ([user_id]) REFERENCES [user]([user_id]),
   FOREIGN KEY (admin_id) REFERENCES [admin]([admin_id])
)

drop table reviews
create table [Order] (
   order_id INT PRIMARY KEY,
   [user_id] INT not null,
   order_date date not null,
   total decimal(10,0) not null,
   [status] varchar(50) not null,
   notes varchar(50) not null,
   FOREIGN KEY ([user_id]) REFERENCES [user]([user_id])
)
drop table reviews
create table [OrderDetail](
   detail_id INT PRIMARY KEY,
   order_id INT not null,
   course_id INT not null,
   price decimal(10,0) not null,
   quantity decimal(10,0) not null,
   FOREIGN KEY (order_id) REFERENCES [Order](order_id),
   FOREIGN KEY (course_id) REFERENCES Course(course_id)

)

create table News(
 news_id INT PRIMARY KEY,
 title varchar(100) not null,
 img_URL varchar(255) not null,
 video_URL varchar(255) not null,
 admin_id INT not null,
 FOREIGN KEY (admin_id) REFERENCES [admin](admin_id)
)

create table Payment(
 payment_id INT PRIMARY KEY,
 payment_date date not null,
 payment_method bit not null,
 order_id INT not null
 FOREIGN KEY (order_id) REFERENCES [Order](order_id)
)