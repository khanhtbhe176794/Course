package entities;

public class Manager implements IActor {
    private Integer id;
    private String name;
    private String email;
    private String password;
    private String address;
    private Boolean gender;
    private String img_URL;
    private String phone;
    private Integer roleId;

    public Manager() {

    }

    public Manager(Integer id, String name, String email, String password, String address, Boolean gender, String img_URL, String phone, Integer roleId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.address = address;
        this.gender = gender;
        this.img_URL = img_URL;
        this.phone = phone;
        this.roleId = roleId;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public String getAddress() {
        return this.address;
    }

    public Boolean getGender() {
        return this.gender;
    }

    public String getImg_URL() {
        return this.img_URL;
    }

    public String getPhone() {
        return this.phone;
    }

    public Integer getRoleId() {
        return this.roleId;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public void setImg_URL(String img_URL) {
        this.img_URL = img_URL;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}
