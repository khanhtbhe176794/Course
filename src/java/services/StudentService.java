package services;

import daos.StudentDAO;
import entities.Student;

import java.util.List;

public class StudentService {
    private static StudentService instance;

    private StudentDAO studentDAO;
    private StudentService(){
        studentDAO = new StudentDAO();
    }

    public static StudentService getInstance(){
        if(instance == null){
            instance = new StudentService();
        }
        return instance;
    }

    public Student getStudentByEmailAndPassword(String email, String password){
        List<Student> result = studentDAO.getStudentByEmailAndPassword(email, password);
        if(result == null || result.isEmpty()){
            return null;
        }
        return result.get(0);
    }
}
