package mappers.impl;

import entities.Manager;
import mappers.RowMapperInterface;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ManagerMapper implements RowMapperInterface<Manager> {
    @Override
    public Manager mapRow(ResultSet rs) {
        try {
            Integer id = rs.getInt("id");
            String name = rs.getString("name");
            String email = rs.getString("email");
            String password = rs.getString("password");
            String address = rs.getString("address");
            Boolean gender = rs.getBoolean("gender");
            String img_URL = rs.getString("img_URL");
            String phone = rs.getString("phone");
            Integer role_id = rs.getInt("role_id");

            return new Manager(id, name, email, password, address, gender, img_URL, phone, role_id);
        } catch (SQLException e) {
            System.out.println("Error in ManagerMapper: " + e.getMessage());
            return null;
        }

    }
}
