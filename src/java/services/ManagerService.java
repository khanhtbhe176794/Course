package services;

import daos.ManagerDAO;
import entities.Manager;

import java.util.List;

public class ManagerService {

    private static ManagerService instance;
    private ManagerDAO managerDAO;
    private ManagerService(){
        managerDAO = new ManagerDAO();
    }

    public static ManagerService getInstance(){
        if(instance == null){
            instance = new ManagerService();
        }
        return instance;
    }

    public Manager getAccountByEmailAndPassword(String username, String password) {
        List<Manager> managers = managerDAO.getManagerByEmailAndPassword(username, password);
        if(managers == null || managers.isEmpty()){
            return null;
        }
        return managers.get(0);
    }

}
