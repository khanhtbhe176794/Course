package daos;

import entities.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDAO extends GenericDAO<Student> {

    private List<Student> listStudent;

    public StudentDAO(){
        listStudent = new ArrayList<>();
        listStudent.add(new Student(1, "nv@gmail.com", "123456", 2));
    }


    public List<Student> getStudentByEmailAndPassword(String email, String password){
        return listStudent.stream()
                .filter(s -> s.getEmail().equals(email) && s.getPassword().equals(password))
                .collect(Collectors.toList());
    }
}
